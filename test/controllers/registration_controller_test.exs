defmodule Peepchat.RegistrationControllerTest do
  use Peepchat.ConnCase

  require Logger

  alias Peepchat.User

  @valid_attrs %{
    email: "mike@example.com",
    password: "fqhi12hrrfasf",
    password_confirmation: "fqhi12hrrfasf"
  }

  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    ur = registration_path(conn, :create)
    Logger.debug "Var value: #{inspect(ur)}"

    conn = post conn, registration_path(conn, :create), %{data: %{type: "user",
      attributes: @valid_attrs
      }}
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(User, %{email: @valid_attrs[:email]})
  end

end